# Shapes

A simple DSL for drawing shapes on a canvas.

```
circle(0, 0, 0.2)
rectangle(-0.5, 0.5, 0.5, -0.5)
```

![An example shapes program.](doc/example.png)

*An example shapes program.*

## Installation

Install with pip (or your package manager of choice): `$ pip install git+https://gitlab.com/da_doomer/shapes`.

## Stand-alone use

You can use Shapes as an interpreter: `shapes my_file.shapes -o my_file.png`.

## Use as a module

You can use Shapes to study and manipulate shapes programs. The module
implements:

- Parsing programs into a tree.

- Sampling of random programs (todo).

- Manipulate programs using their trees (todo).

## Caveats

The semantics of Shapes are implemented with Pillow. In particular, the output
images are raster images.
