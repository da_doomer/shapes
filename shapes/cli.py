"""A command-line interpreter for the shapes DSL."""
# Native imports
from pathlib import Path
import argparse

# Local imports
import shapes.program

# External imports
import lark


def interpreter():
    """A command-line interpreter for the shapes DSL."""
    parser = argparse.ArgumentParser(
            description='Execute a Shapes program.'
        )
    parser.add_argument('FILE', type=Path, help="Shapes file.")
    parser.add_argument(
            '-o',
            '--output',
            help="Output file.",
            type=Path,
            required=True,
        )
    parser.add_argument(
            '--tree',
            help="Output file.",
            type=Path,
            default=None,
        )
    args = parser.parse_args()

    # Open the input file
    with open(args.FILE, "rt") as f_p:
        program = shapes.program.Program("".join(f_p).strip())

    # Save the output to the output file
    program.save_output(args.output)

    # If given, save the parse tree
    if args.tree is not None:
        lark.tree.pydot__tree_to_png(program.parse_tree, args.tree)
