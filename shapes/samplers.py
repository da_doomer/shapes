"""Sampling methods for grammars."""
# Native imports
import random
from typing import Union

# External imports
import lark
import lark.reconstruct

# Local imports
import shapes.grammar


def random_sample(
        parser: lark.Lark,
        symbol: str,
        ) -> Union[lark.Tree, lark.Token]:
    """Sample a tree expanding the rules in the grammar with equal
    probability, starting from the given symbol."""
    grammar = shapes.grammar.Grammar(parser)

    # Choose a rule to replace the symbol
    rule = random.choice(grammar[symbol])
    children = list()
    for _symbol in rule.expansion:
        if _symbol.name not in grammar.terminals:
            tree = random_sample(parser, _symbol.name)
            children.append(tree)
        else:
            if _symbol.name == "NUMBER":
                value = str(round(random.random(), 2))
            elif _symbol.name == "SIGNED_NUMBER":
                value = str(round(random.random()*2-1, 2))
            elif _symbol.name == "NEWLINE":
                value = "\n"
            else:
                continue
            tree = lark.Token(_symbol.name, value)
            children.append(tree)
    tree = lark.Tree(symbol, children)
    return tree


if __name__ == "__main__":
    """Prints a random program to stdout."""
    parser = shapes.grammar.Parser()
    tree = random_sample(parser, shapes.grammar.START)
    program = lark.reconstruct.Reconstructor(parser).reconstruct(tree)
    print(program)
