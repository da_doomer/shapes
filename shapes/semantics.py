"""Semantics for the Shapes Language."""
# Native imports
import copy

# External imports
import lark
import numpy as np
from PIL import Image
from PIL import ImageDraw


def coordinate_to_pixels(
        x: float,
        y: float,
        width: int,
        height: int
        ) -> tuple[int, int]:
    """Returns the given coordinate transformed to pixel scale. This
    operation depends on the size of the canvas."""
    new_x = width//2 + int(width/2*x)
    new_y = height//2 - int(width/2*y)
    return (new_x, new_y)


def execute_on_array(
        tree: lark.Tree,
        array: np.ndarray,
        ) -> np.ndarray:
    """Add the execution of the tree to the given pixel array, returning
    a new array. The shape of the canvas is inferred from the given array.

    The canvas of a Shapes program has a range in [-1, 1] for the horizontal
    axis, and the vertical axis is expressed in the scale of the horizontal
    axis.

    Example: for a canvas of 640 horizontal and 480 vertical pixels, the
    coordinate (1.0, 0.0) is (640, 240) in pixel scale; and the coordinate
    (-1.0, 1.0) is (0, 640).
    """
    # Build a new array to draw on
    array = copy.deepcopy(array)
    height, width = array.shape[:2]
    image = Image.fromarray(array)
    drawer = ImageDraw.Draw(image)

    # Fix parameters
    stroke_width = int(width*0.015)
    outline = "blue"

    # Execute the tree
    class Interpreter(lark.visitors.Interpreter):
        """Implements recursive Shapes semantics for a parse tree."""
        def posnum(self, tree):
            """A `posnum` node gets mapped to a Python float."""
            return float(tree.children[0])

        def num(self, tree):
            """A `num` node gets mapped to a Python float."""
            return float(tree.children[0])

        def rectangle(self, tree):
            """Rectanngles are drawn to the array."""
            x1, y1, x2, y2 = [self.visit(child) for child in tree.children]
            x1, y1 = coordinate_to_pixels(x1, y1, width, height)
            x2, y2 = coordinate_to_pixels(x2, y2, width, height)
            x1, x2 = sorted((x1, x2))
            y1, y2 = sorted((y1, y2))
            drawer.rectangle(
                    (x1, y1, x2, y2),
                    outline=outline,
                    width=stroke_width
                )

        def circle(self, tree):
            """Circles are drawn to the array."""
            x, y, r = [self.visit(child) for child in tree.children]
            x1 = x-r
            y1 = y-r
            x2 = x+r
            y2 = y+r
            x1, y1 = coordinate_to_pixels(x1, y1, width, height)
            x2, y2 = coordinate_to_pixels(x2, y2, width, height)
            x1, x2 = sorted((x1, x2))
            y1, y2 = sorted((y1, y2))
            drawer.chord(
                    [x1, y1, x2, y2],
                    0,
                    360.0,
                    outline=outline,
                    width=stroke_width
                )

    Interpreter().visit(tree)

    array = np.asarray(image)
    return array
