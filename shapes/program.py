"""Object-oriented API for Shapes semantics."""
# Native imports
from dataclasses import dataclass
from pathlib import Path

# External imports
import lark
import numpy as np
from PIL import Image

# Local imports
import shapes.grammar
import shapes.semantics


@dataclass(order=True, frozen=True)
class Program:
    """This class implements an object-oriented API for Shapes programs."""
    source_code: str
    width: int = 640
    height: int = 480

    @property
    def parse_tree(self) -> lark.Tree:
        """Return the lark parse tree of the program."""
        parser = shapes.grammar.Parser()
        tree = parser.parse(self.source_code)
        return tree

    @property
    def pixels(self) -> np.ndarray:
        """Return an array encoding the output pixels."""
        tree = self.parse_tree
        blank_array = np.asarray(Image.new(
                "RGB",
                (self.width, self.height),
                (255, 255, 255),
            ))
        array = shapes.semantics.execute_on_array(tree, blank_array)
        return array

    def save_output(self, path: Path):
        """Save the output pixels to the given path."""
        image = Image.fromarray(self.pixels)
        image.save(path)
