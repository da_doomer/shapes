"""Defines the Shapes grammar in Extended Backus-Naur Form, for use with
Lark."""
# Native imports
from collections import defaultdict

# External imports
import lark

EBNF_GRAMMAR = r"""%ignore " "
%import common.NUMBER
%import common.SIGNED_NUMBER
%import common.NEWLINE

start: command
    | command NEWLINE start

command: circle | rectangle

circle: "circle" "(" num "," num "," posnum ")"
rectangle: "rectangle" "(" num "," num "," num "," num ")"

posnum: NUMBER
num: NUMBER | SIGNED_NUMBER
"""

START = "start"


class Grammar(dict[str, tuple[str, ...]]):
    """Dict interface for the rules of a lark parser."""
    def __init__(self, parser: lark.Lark):
        super().__init__()
        temp_dict = defaultdict(list)
        for rule in parser.rules:
            temp_dict[rule.origin.name].append(rule)
        self.terminals = {
                terminal.name: terminal.pattern.value
                for terminal in parser.terminals
            }
        for k, v in temp_dict.items():
            self[k] = tuple(v)


class Parser(lark.Lark):
    """Instantiates a lark parser with the Shapes grammar and the given keyword
    arguments."""
    def __init__(self, **kwargs):
        super().__init__(EBNF_GRAMMAR, **kwargs)
