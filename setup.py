#!/usr/bin/env python

from distutils.core import setup

setup(name='Shapes',
      version='0.1',
      description='A DSL for drawing shapes.',
      packages=['shapes'],
      entry_points={'console_scripts': ['shapes=shapes.__main__:interpreter']},
      )
